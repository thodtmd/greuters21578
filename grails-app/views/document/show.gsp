
<%@ page import="com.reuters.Document" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'document.label', default: 'Document')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-document" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-document" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list document">

                <g:if test="${documentInstance?.reutersNewId}">
                    <li class="fieldcontain">
                        <span id="reutersNewId-label" class="property-label"><g:message code="document.reutersNewId.label" default="Reuters New Id" /></span>

                        <span class="property-value" aria-labelledby="reutersNewId-label"><g:fieldValue bean="${documentInstance}" field="reutersNewId"/></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.reutersOldId}">
                    <li class="fieldcontain">
                        <span id="reutersOldId-label" class="property-label"><g:message code="document.reutersOldId.label" default="Reuters Old Id" /></span>

                        <span class="property-value" aria-labelledby="reutersOldId-label"><g:fieldValue bean="${documentInstance}" field="reutersOldId"/></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.dateTime}">
                    <li class="fieldcontain">
                        <span id="dateTime-label" class="property-label"><g:message code="document.dateTime.label" default="Date Time" /></span>

                        <span class="property-value" aria-labelledby="dateTime-label"><g:formatDate date="${documentInstance?.dateTime}" /></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.reutersCgiSplit}">
                    <li class="fieldcontain">
                        <span id="reutersCgiSplit-label" class="property-label"><g:message code="document.reutersCgiSplit.label" default="Reuters Cgi Split" /></span>

                        <span class="property-value" aria-labelledby="reutersCgiSplit-label"><g:fieldValue bean="${documentInstance}" field="reutersCgiSplit"/></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.reutersLewisSplit}">
                    <li class="fieldcontain">
                        <span id="reutersLewisSplit-label" class="property-label"><g:message code="document.reutersLewisSplit.label" default="Reuters Lewis Split" /></span>

                        <span class="property-value" aria-labelledby="reutersLewisSplit-label"><g:fieldValue bean="${documentInstance}" field="reutersLewisSplit"/></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.reutersTopics}">
                    <li class="fieldcontain">
                        <span id="reutersTopics-label" class="property-label"><g:message code="document.reutersTopics.label" default="Reuters Topics" /></span>

                        <span class="property-value" aria-labelledby="reutersTopics-label"><g:fieldValue bean="${documentInstance}" field="reutersTopics"/></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.textType}">
                    <li class="fieldcontain">
                        <span id="textType-label" class="property-label"><g:message code="document.textType.label" default="Text Type" /></span>

                        <span class="property-value" aria-labelledby="textType-label"><g:fieldValue bean="${documentInstance}" field="textType"/></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.textAuthor}">
                    <li class="fieldcontain">
                        <span id="textAuthor-label" class="property-label"><g:message code="document.textAuthor.label" default="Text Author" /></span>

                        <span class="property-value" aria-labelledby="textAuthor-label"><g:fieldValue bean="${documentInstance}" field="textAuthor"/></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.textDateline}">
                    <li class="fieldcontain">
                        <span id="textDateline-label" class="property-label"><g:message code="document.textDateline.label" default="Text Dateline" /></span>

                        <span class="property-value" aria-labelledby="textDateline-label"><g:fieldValue bean="${documentInstance}" field="textDateline"/></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.textTitle}">
                    <li class="fieldcontain">
                        <span id="textTitle-label" class="property-label"><g:message code="document.textTitle.label" default="Text Title" /></span>

                        <span class="property-value" aria-labelledby="textTitle-label"><g:fieldValue bean="${documentInstance}" field="textTitle"/></span>

                    </li>
                </g:if>

				<g:if test="${documentInstance?.exchanges}">
				<li class="fieldcontain">
					<span id="exchanges-label" class="property-label"><g:message code="document.exchanges.label" default="Exchanges" /></span>
					
						<g:each in="${documentInstance.exchanges}" var="e">
						<span class="property-value" aria-labelledby="exchanges-label"><g:link controller="exchange" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${documentInstance?.orgs}">
				<li class="fieldcontain">
					<span id="orgs-label" class="property-label"><g:message code="document.orgs.label" default="Orgs" /></span>
					
						<g:each in="${documentInstance.orgs}" var="o">
						<span class="property-value" aria-labelledby="orgs-label"><g:link controller="organization" action="show" id="${o.id}">${o?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${documentInstance?.people}">
				<li class="fieldcontain">
					<span id="people-label" class="property-label"><g:message code="document.people.label" default="People" /></span>
					
						<g:each in="${documentInstance.people}" var="p">
						<span class="property-value" aria-labelledby="people-label"><g:link controller="person" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${documentInstance?.places}">
				<li class="fieldcontain">
					<span id="places-label" class="property-label"><g:message code="document.places.label" default="Places" /></span>
					
						<g:each in="${documentInstance.places}" var="p">
						<span class="property-value" aria-labelledby="places-label"><g:link controller="place" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${documentInstance?.topics}">
				<li class="fieldcontain">
					<span id="topics-label" class="property-label"><g:message code="document.topics.label" default="Topics" /></span>
					
						<g:each in="${documentInstance.topics}" var="t">
						<span class="property-value" aria-labelledby="topics-label"><g:link controller="topic" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>

                <g:if test="${documentInstance?.textBody}">
                    <li class="fieldcontain">
                        <span id="textBody-label" class="property-label"><g:message code="document.textBody.label" default="Text Body" /></span>

                        <span class="property-value" aria-labelledby="textBody-label"><g:fieldValue bean="${documentInstance}" field="textBody"/></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.unknown}">
                    <li class="fieldcontain">
                        <span id="unknown-label" class="property-label"><g:message code="document.unknown.label" default="Unknown" /></span>

                        <span class="property-value" aria-labelledby="unknown-label"><g:fieldValue bean="${documentInstance}" field="unknown"/></span>

                    </li>
                </g:if>

                <g:if test="${documentInstance?.mkNote}">
                    <li class="fieldcontain">
                        <span id="mkNote-label" class="property-label"><g:message code="document.mkNote.label" default="Mk Note" /></span>

                        <span class="property-value" aria-labelledby="mkNote-label"><g:fieldValue bean="${documentInstance}" field="mkNote"/></span>

                    </li>
                </g:if>

            </ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${documentInstance?.id}" />
					<g:link class="edit" action="edit" id="${documentInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
