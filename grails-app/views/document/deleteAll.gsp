<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'document.label', default: 'Document')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>
<body>
<a href="#delete-all" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
    </ul>
</div>
<div id="delete-all" class="content scaffold-edit" role="main">
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form method="post" >
        <fieldset class="form">
            <label><g:message code="document.totalDocuments.message" args="[totalDocs]"/></label>
        </fieldset>
        <fieldset class="buttons">
            <g:actionSubmit class="delete" action="doDeleteAll" value="${message(code: 'default.button.deleteAll.label', default: 'Delete All')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
        </fieldset>
    </g:form>
</div>
</body>
</html>