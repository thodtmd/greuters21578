<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'document.label', default: 'Document')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>
<body>
<a href="#execute-sql" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
    </ul>
</div>
<div id="execute-sql" class="content scaffold-edit" role="main">
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form method="post" >
        <fieldset class="form">
            <div class="fieldcontain">
                <label for="queryText">
                    <g:message code="sql.title" default="SQL query" />
                </label>
                <g:textArea name="queryText" cols="80" rows="5" style="width: 500px; font-family: monospace" value="${originalQuery ?: ""}"/>
            </div>
        </fieldset>
        <g:if test="${hasResults}">
            <ol class="property-list document">
                <li class="fieldcontain">
                    <span id="sqltime-label" class="property-label"><g:message code="sql.time.label" default="Execution time" /></span>

                    <span class="property-value" aria-labelledby="sqltime-label">${time} ms</span>
                </li>
                <li class="fieldcontain">
                    <span id="sqlresults-label" class="property-label"><g:message code="sql.result.label" default="SQL query result" /></span>

                    <span class="property-value" aria-labelledby="sqlresults-label">
                        <g:each in="${results}" var="result">
                            ${result.encodeAsHTML() + "<br/>\n"}
                        </g:each>
                    </span>
                </li>
            </ol>
        </g:if>
        <fieldset class="buttons">
            <g:actionSubmit class="list" action="doExecuteSql" value="${message(code: 'default.button.execute.label', default: 'Execute')}" />
        </fieldset>
    </g:form>
</div>
</body>
</html>