<%@ page import="com.reuters.Document" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'document.label', default: 'Document')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<a href="#load-data" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="delete" action="deleteAll"><g:message code="document.deleteAll.label"/></g:link></li>
    </ul>
</div>
<div id="load-data" class="content scaffold-edit" role="main">
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form method="post" >
        <fieldset class="form">
            <div class="fieldcontain required">
                <label for="amount">
                    <g:message code="document.amount.label" default="Amount" />
                    <span class="required-indicator">*</span>
                </label>
                <g:field name="amount" type="number" value="0" required=""/>
            </div>
        </fieldset>
        <fieldset class="buttons">
            <g:actionSubmit class="create" action="doLoad" value="${message(code: 'default.button.load.label', default: 'Load')}" />
        </fieldset>
    </g:form>
</div>
</body>
</html>