<%@ page import="com.reuters.Document" %>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'reutersNewId', 'error')} required">
    <label for="id">
        <g:message code="document.reutersNewId.label" default="Reuters New Id" />
        <span class="required-indicator">*</span>
    </label>
    <g:field name="reutersNewId" type="number" value="${documentInstance.reutersNewId ?: 0}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'reutersOldId', 'error')} required">
    <label for="reutersOldId">
        <g:message code="document.reutersOldId.label" default="Reuters Old Id" />
        <span class="required-indicator">*</span>
    </label>
    <g:field name="reutersOldId" type="number" value="${documentInstance.reutersOldId}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'dateTime', 'error')} required">
    <label for="dateTime">
        <g:message code="document.dateTime.label" default="Date Time" />
        <span class="required-indicator">*</span>
    </label>
    <g:datePicker name="dateTime" precision="day"  value="${documentInstance?.dateTime}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'reutersCgiSplit', 'error')} required">
    <label for="reutersCgiSplit">
        <g:message code="document.reutersCgiSplit.label" default="Reuters Cgi Split" />
        <span class="required-indicator">*</span>
    </label>
    <g:select name="reutersCgiSplit" from="${com.reuters.attributes.CgiSplit?.values()}" keys="${com.reuters.attributes.CgiSplit.values()*.name()}" required="" value="${documentInstance?.reutersCgiSplit?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'reutersLewisSplit', 'error')} required">
    <label for="reutersLewisSplit">
        <g:message code="document.reutersLewisSplit.label" default="Reuters Lewis Split" />
        <span class="required-indicator">*</span>
    </label>
    <g:select name="reutersLewisSplit" from="${com.reuters.attributes.LewisSplit?.values()}" keys="${com.reuters.attributes.LewisSplit.values()*.name()}" required="" value="${documentInstance?.reutersLewisSplit?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'reutersTopics', 'error')} required">
    <label for="reutersTopics">
        <g:message code="document.reutersTopics.label" default="Reuters Topics" />
        <span class="required-indicator">*</span>
    </label>
    <g:select name="reutersTopics" from="${com.reuters.attributes.Topics?.values()}" keys="${com.reuters.attributes.Topics.values()*.name()}" required="" value="${documentInstance?.reutersTopics?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'textType', 'error')} required">
    <label for="textType">
        <g:message code="document.textType.label" default="Text Type" />
        <span class="required-indicator">*</span>
    </label>
    <g:select name="textType" from="${com.reuters.attributes.Type?.values()}" keys="${com.reuters.attributes.Type.values()*.name()}" required="" value="${documentInstance?.textType?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'textAuthor', 'error')} ">
	<label for="textAuthor">
		<g:message code="document.textAuthor.label" default="Text Author" />
		
	</label>
	<g:textField name="textAuthor" maxlength="100" value="${documentInstance?.textAuthor}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'textDateline', 'error')} ">
	<label for="textDateline">
		<g:message code="document.textDateline.label" default="Text Dateline" />
		
	</label>
	<g:textField name="textDateline" maxlength="100" value="${documentInstance?.textDateline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'textTitle', 'error')} ">
	<label for="textTitle">
		<g:message code="document.textTitle.label" default="Text Title" />
		
	</label>
	<g:textField name="textTitle" maxlength="200" value="${documentInstance?.textTitle}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'exchanges', 'error')} ">
	<label for="exchanges">
		<g:message code="document.exchanges.label" default="Exchanges" />
		
	</label>
	<g:select name="exchanges" from="${com.reuters.categories.Exchange.list()}" multiple="multiple" optionKey="id" size="5" value="${documentInstance?.exchanges*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'orgs', 'error')} ">
	<label for="orgs">
		<g:message code="document.orgs.label" default="Orgs" />
		
	</label>
	<g:select name="orgs" from="${com.reuters.categories.Organization.list()}" multiple="multiple" optionKey="id" size="5" value="${documentInstance?.orgs*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'people', 'error')} ">
	<label for="people">
		<g:message code="document.people.label" default="People" />
		
	</label>
	<g:select name="people" from="${com.reuters.categories.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${documentInstance?.people*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'places', 'error')} ">
	<label for="places">
		<g:message code="document.places.label" default="Places" />
		
	</label>
	<g:select name="places" from="${com.reuters.categories.Place.list()}" multiple="multiple" optionKey="id" size="5" value="${documentInstance?.places*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'topics', 'error')} ">
	<label for="topics">
		<g:message code="document.topics.label" default="Topics" />
		
	</label>
	<g:select name="topics" from="${com.reuters.categories.Topic.list()}" multiple="multiple" optionKey="id" size="5" value="${documentInstance?.topics*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'textBody', 'error')} ">
    <label for="textBody">
        <g:message code="document.textBody.label" default="Text Body" />

    </label>
    <g:textArea name="textBody" cols="40" rows="5" value="${documentInstance?.textBody}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'unknown', 'error')} ">
    <label for="unknown">
        <g:message code="document.unknown.label" default="Unknown" />

    </label>
    <g:textArea name="unknown" cols="40" rows="5" maxlength="500" value="${documentInstance?.unknown}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'mkNote', 'error')} ">
    <label for="mkNote">
        <g:message code="document.mkNote.label" default="Mk Note" />

    </label>
    <g:textArea name="mkNote" cols="40" rows="5" maxlength="500" value="${documentInstance?.mkNote}"/>
</div>