package com.reuters

import com.reuters.attributes.CgiSplit
import com.reuters.attributes.LewisSplit
import com.reuters.attributes.Topics
import com.reuters.attributes.Type
import com.reuters.categories.*

class Document {

    static hasMany = [exchanges: Exchange, orgs: Organization, people: Person, places: Place, topics: Topic]

    Integer reutersNewId
    Integer reutersOldId
    Topics reutersTopics
    LewisSplit reutersLewisSplit
    CgiSplit reutersCgiSplit

    Date dateTime
    String unknown
    String mkNote

    Type textType
    String textAuthor
    String textBody
    String textDateline
    String textTitle

    static constraints = {
        unknown(nullable: true)
        mkNote(nullable: true)
        textBody(nullable: true)
        textAuthor(nullable: true, maxSize: 100)
        textDateline(nullable: true, maxSize: 100)
        textTitle(nullable: true, maxSize: 200)
        unknown(maxSize: 500)
        mkNote(maxSize: 500)
    }

    static mapping = {
        id(generator: MappingHelper.identityGeneratorType)
        reutersTopics(sqlType: 'char(1)')
        reutersLewisSplit(sqlType: 'char(1)')
        reutersCgiSplit(sqlType: 'char(1)')
        textType(sqlType: 'char(1)')
        textBody(sqlType: MappingHelper.textType)
        version(false)
    }

}
