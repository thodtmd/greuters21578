package com.reuters.categories

import com.reuters.Document

/**
 * &lt;TOPICS&gt; Category type.
 */
class Topic extends Category {

    static belongsTo = Document
    static hasMany = [documents: Document]

}
