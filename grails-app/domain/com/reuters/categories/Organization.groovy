package com.reuters.categories

import com.reuters.Document

/**
 * &lt;ORGS&gt; Category type.
 */
class Organization extends Category {

    static belongsTo = Document
    static hasMany = [documents: Document]

}
