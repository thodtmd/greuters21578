package com.reuters.categories

import com.reuters.Document

/**
 * &lt;PLACES&gt; Category type.
 */
class Place extends Category {

    static belongsTo = Document
    static hasMany = [documents: Document]

}
