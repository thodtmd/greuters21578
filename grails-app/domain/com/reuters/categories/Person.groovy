package com.reuters.categories

import com.reuters.Document

/**
 * &lt;PEOPLE&gt; Category type.
 */
class Person extends Category {

    static belongsTo = Document
    static hasMany = [documents: Document]

}
