package com.reuters.categories

import com.reuters.Document

/**
 * &lt;EXCHANGES&gt; Category type.
 */
class Exchange extends Category {

    static belongsTo = Document
    static hasMany = [documents: Document]

}
