package com.reuters.attributes

/**
 * Enum for &lt;REUTERS TOPICS=""&gt; attribute.
 */
public enum Topics {

    YES("Y"),
    NO("N"),
    BYPASS("B")

    final String id

    Topics(String id) {
        this.id = id
    }

    static Topics fromSGMLString(String string) {
        switch (string) {
            case "YES": return YES
            case "NO": return NO
            case "BYPASS": return BYPASS
            default: return null
        }
    }

}