package com.reuters.attributes

/**
 * Enum for &lt;REUTERS LEWISSPLIT=""&gt; attribute.
 */
public enum LewisSplit {

    TRAIN("R"),
    TEST("T"),
    NOT_USED("N")

    final String id

    LewisSplit(String id) {
        this.id = id
    }

    static LewisSplit fromSGMLString(String string) {
        switch (string) {
            case "TRAIN": return TRAIN
            case "TEST": return TEST
            case "NOT-USED": return NOT_USED
            default: return null
        }
    }

}