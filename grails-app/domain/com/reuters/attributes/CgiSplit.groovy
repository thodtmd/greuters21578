package com.reuters.attributes

/**
 * Enum for &lt;REUTERS CGISPLIT=""&gt; attribute.
 */
public enum CgiSplit {

    TRAINING_SET("T"),
    PUBLISHED_TESTSET("P")

    final String id

    CgiSplit(String id) {
        this.id = id
    }

    static CgiSplit fromSGMLString(String string) {
        switch (string) {
            case "TRAINING-SET": return TRAINING_SET
            case "PUBLISHED-TESTSET": return PUBLISHED_TESTSET
            default: return null
        }
    }

}