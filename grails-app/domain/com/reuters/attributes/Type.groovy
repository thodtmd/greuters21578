package com.reuters.attributes

/**
 * Enum for &lt;TEXT TYPE=""&gt; attribute.
 */
public enum Type {

    NORM("N"),
    BRIEF("B"),
    UNPROC("U")

    final String id

    Type(final String id) {
        this.id = id
    }

    static Type fromSGMLString(String string) {
        switch (string) {
            case "NORM": return NORM
            case "BRIEF": return BRIEF
            case "UNPROC": return UNPROC
            default: return null
        }
    }

}