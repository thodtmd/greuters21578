import org.codehaus.groovy.grails.orm.hibernate.cfg.DefaultGrailsDomainConfiguration
import org.hibernate.MappingException
import org.hibernate.mapping.Column
import org.hibernate.mapping.ForeignKey
import org.hibernate.mapping.Table

class SaneForeignKeyNameConfiguration extends DefaultGrailsDomainConfiguration {

    void secondPassCompileForeignKeys(Table table, Set done) throws MappingException {
        overrideForeignKeyNames(table)
        super.secondPassCompileForeignKeys(table, done)
    }

    void overrideForeignKeyNames(Table table) {
        table.foreignKeyIterator.each { ForeignKey fk ->
            def fromTableName = table.name
            def fromColumnName = ((Column) fk.columnIterator.next()).name
            def foreignKeyName = "${fromTableName}_${fromColumnName}_FK".toLowerCase()
            fk.name = foreignKeyName
        }
    }

}
