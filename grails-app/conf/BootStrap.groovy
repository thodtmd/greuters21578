import com.reuters.categories.*

class BootStrap {

    def categoryService
    def grailsApplication

    def init = { servletContext ->
        checkCategories()
    }
    def destroy = {
    }

    void checkCategories() {
        log.info("Checking categories...")
        def reutersFolder = grailsApplication.config.reutersFolder.toString()
        reutersFolder = reutersFolder.endsWith(File.separator) ? reutersFolder : reutersFolder + File.separator
        if (Exchange.count == 0) {
            def file = new File(reutersFolder + "all-exchanges-strings.lc.txt")
            log.info("Adding exchanges from ${file.path}")
            categoryService.persistCategories(categoryService.getCategoriesFromFile(file, Exchange))
        }
        if (Organization.count == 0) {
            def file = new File(reutersFolder + "all-orgs-strings.lc.txt")
            log.info("Adding orgs from ${file.path}")
            categoryService.persistCategories(categoryService.getCategoriesFromFile(file, Organization))
        }
        if (Person.count == 0) {
            def file = new File(reutersFolder + "all-people-strings.lc.txt")
            log.info("Adding people from ${file.path}")
            categoryService.persistCategories(categoryService.getCategoriesFromFile(file, Person))
        }
        if (Place.count == 0) {
            def file = new File(reutersFolder + "all-places-strings.lc.txt")
            log.info("Adding places from ${file.path}")
            categoryService.persistCategories(categoryService.getCategoriesFromFile(file, Place))
        }
        if (Topic.count == 0) {
            def file = new File(reutersFolder + "all-topics-strings.lc.txt")
            log.info("Adding topics from ${file.path}")
            categoryService.persistCategories(categoryService.getCategoriesFromFile(file, Topic))
        }
        log.info("Done checking categories")
    }

}
