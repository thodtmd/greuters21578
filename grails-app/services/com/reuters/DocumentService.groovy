package com.reuters

import com.reuters.attributes.CgiSplit
import com.reuters.attributes.LewisSplit
import com.reuters.attributes.Topics
import com.reuters.attributes.Type
import groovy.sql.GroovyRowResult
import groovy.util.slurpersupport.GPathResult
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin
import org.hibernate.SessionFactory
import org.springframework.beans.factory.InitializingBean
import org.xml.sax.InputSource

import javax.sql.DataSource

import com.reuters.categories.*

import static com.reuters.DocumentUtils.entityText

class DocumentService implements InitializingBean {

    static transactional = false

    GrailsApplication grailsApplication
    ConfigObject config
    SessionFactory sessionFactory
    DataSource dataSource
    TreeSet<Document> documents = new TreeSet<Document>(DocumentUtils.DOCUMENT_ID_COMPARATOR)

    @Override
    void afterPropertiesSet() {
        config = grailsApplication.config
    }

    void deleteAllDocuments() {
        log.info("Deleting all documents...")

        if (config.clearCacheOnDelete == true) {
            documents.clear()
        }

        DocumentUtils.deleteAllDocumentsWithSQL(dataSource)
        sessionFactory.cache.evictEntityRegion(Document)
        DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP.get().clear()
        sessionFactory.currentSession.clear()

        log.info("${Document.count()} documents currently in database")
    }

    List<GroovyRowResult> executeSQL(String query) {
        log.info("Executing SQL '$query'...")
        def results = DocumentUtils.executeSQLInTransaction(dataSource, query)
        log.info("Query returned ${results.size()} results")
        return results
    }

    void importDocuments(Integer amount) {
        int total = amount >= 0 ? amount : 1000
        int batchSize = config.batchSize instanceof Number ? config.batchSize.intValue() : 100
        int lastNewId = lastReutersNewId
        if (!documents) {
            documents.addAll(gatherAllDocuments(DocumentUtils.listFiles(config.reutersFolder, "*.sgm")))
        }
        def clones = DocumentUtils.buildClones(documents, lastNewId, total)
        long time = System.currentTimeMillis()
        Document.withTransaction() {
            batchSave(clones, batchSize)
        }
        log.info("Total transaction runtime: ${System.currentTimeMillis() - time} ms")
    }

    protected Collection<Document> gatherAllDocuments(Collection<File> files) {
        log.info("Started gathering documents...")
        def result = [] as Set
        for (def file : files) {
            def fileSet = [] as Set
            processSGMLFile(file, fileSet, "UTF-8")
            result.addAll(fileSet)

            log.info("Processed file ${file.absolutePath}")
        }
        log.info("Gathered ${result.size()} documents")
        return result
    }

    protected void processSGMLFile(File sgmlFile, Collection<Document> output, String encoding) {
        def stream = null
        try {
            stream = DocumentUtils.createSGMLPreparser(sgmlFile, encoding)
            processXMLInputSource(DocumentUtils.newInputSource(stream, encoding), output)
        } finally {
            stream?.close()
        }
    }

    protected void processXMLInputSource(InputSource source, Collection<Document> output) {
        def xml = new XmlSlurper().parse(source)
        def reuters = xml."REUTERS"
        for (GPathResult docNode : reuters) {
            def doc = new Document()

            populateDocumentHeader(doc, docNode)
            populateDocumentCollections(doc, docNode)
            populateDocumentTextSection(doc, docNode)

            output.add(doc)
        }
    }

    protected void batchSave(Collection<Document> docs, int batchSize) {
        log.info("Persisting ${docs.size()} documents in batches of ${batchSize}...")
        long time = System.currentTimeMillis()

        def docsIterator = docs.iterator()
        for (int i = 0; docsIterator.hasNext(); i++) {
            docsIterator.next().save(failOnError: true)

            if ((i + 1) % batchSize == 0) {
                log.info("${i + 1} documents persisted")
                cleanupSession()
            }
        }

        log.info("Finished persisting ${docs.size()} documents in ${System.currentTimeMillis() - time} ms")
    }

    protected void cleanupSession() {
        DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP.get().clear()

        def session = sessionFactory.currentSession
        session.flush()
        session.clear()
    }

    private void populateDocumentHeader(Document document, GPathResult documentNode) {
        document.reutersNewId = Integer.valueOf(entityText(documentNode.@"NEWID"))
        document.reutersOldId = Integer.valueOf(entityText(documentNode.@"OLDID"))
        document.reutersTopics = Topics.fromSGMLString(entityText(documentNode.@"TOPICS"))
        document.reutersLewisSplit = LewisSplit.fromSGMLString(entityText(documentNode.@"LEWISSPLIT"))
        document.reutersCgiSplit = CgiSplit.fromSGMLString(entityText(documentNode.@"CGISPLIT"))

        document.dateTime = DocumentUtils.parseReutersDate(entityText(documentNode."DATE"))
        document.mkNote = entityText(documentNode."MKNOTE")
        document.unknown = entityText(documentNode."UNKNOWN")
    }

    private void populateDocumentCollections(Document document, GPathResult documentNode) {
        def topicsNode = (GPathResult) documentNode."TOPICS".find()
        topicsNode."D"?.each() { GPathResult topicNode ->
            def topic = Topic.findByName(entityText(topicNode)?.toLowerCase(), [cache: true])
            if (topic) document.addToTopics(topic)
        }
        def placesNode = (GPathResult) documentNode."PLACES".find()
        placesNode."D"?.each() { GPathResult placeNode ->
            def place = Place.findByName(entityText(placeNode)?.toLowerCase(), [cache: true])
            if (place) document.addToPlaces(place)
        }
        def peopleNode = (GPathResult) documentNode."PEOPLE".find()
        peopleNode."D"?.each() { GPathResult personNode ->
            def person = Person.findByName(entityText(personNode)?.toLowerCase(), [cache: true])
            if (person) document.addToPeople(person)
        }
        def orgsNode = (GPathResult) documentNode."ORGS".find()
        orgsNode."D"?.each() { GPathResult orgNode ->
            def org = Organization.findByName(entityText(orgNode)?.toLowerCase(), [cache: true])
            if (org) document.addToOrgs(org)
        }
        def exchangesNode = (GPathResult) documentNode."EXCHANGES".find()
        exchangesNode."D"?.each() { GPathResult exchangeNode ->
            def exchange = Exchange.findByName(entityText(exchangeNode)?.toLowerCase(), [cache: true])
            if (exchange) document.addToExchanges(exchange)
        }
    }

    private void populateDocumentTextSection(Document document, GPathResult documentNode) {
        def textNode = documentNode."TEXT".find()

        document.textType = Type.fromSGMLString(entityText(textNode.@"BODY")) ?: Type.NORM
        document.textAuthor = entityText(textNode."AUTHOR")
        document.textTitle = entityText(textNode."TITLE")
        document.textDateline = entityText(textNode."DATELINE", false)
        document.textBody = entityText(textNode."BODY", false)
    }

    protected int getLastReutersNewId() {
        def doc = Document.withCriteria {
            order("id", "desc")
            maxResults(1)
        }
        return doc ? doc.first().reutersNewId : 0
    }

}
