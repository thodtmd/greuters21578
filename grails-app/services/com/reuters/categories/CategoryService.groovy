package com.reuters.categories

class CategoryService {

    Set<?> getCategoriesFromFile(File file, Class<? extends Category> categoryClass) {
        def result = [] as LinkedHashSet
        file.eachLine() { String line ->
            def item = (Category) categoryClass.newInstance()
            item.name = line.trim()
            result.add(item)
        }
        return result
    }

    void persistCategories(Collection<? extends Category> categories) {
        categories.each() { Category category ->
            category.save(failOnError: true)
        }
    }

}
