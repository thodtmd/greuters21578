Reuters-21578 database benchmark application

Instructions to run:

1. Obtain a copy of Reuters-21578 corpus from http://www.daviddlewis.com/resources/testcollections/reuters21578/reuters21578.tar.gz
2. Unpack it to reuters21578 folder or any other location
3. Obtain a copy of PostgreSQL and/or Oracle JDBC driver and put it to lib folder
4. Launch via grails run-app.

Recommended JVM settings: at least -Xmx1024m

If the program cannot find the Reuters corpus, you may specify it explicitly via -Dreuters.folder="[full path]" JVM parameter.