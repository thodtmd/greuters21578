package org.hibernate.dialect;

import org.hibernate.id.PersistentIdentifierGenerator;
import org.hibernate.id.SequenceGenerator;
import org.hibernate.type.*;

import java.util.Map;
import java.util.Properties;
import java.util.WeakHashMap;

public class CustomOracleDialect extends Oracle10gDialect {

    public static final String PARAMETERS = "MINVALUE 1 MAXVALUE %s INCREMENT BY 1 START WITH 1 NOCACHE NOCYCLE";

    private static final String DEFAULT_MAX_VALUE = "9999999999999999999999999999";

    private static final Map<Class<?>, Number> MAX_VALUES = new WeakHashMap<Class<?>, Number>();

    static {
        MAX_VALUES.put(ByteType.class, Byte.MAX_VALUE);
        MAX_VALUES.put(ShortType.class, Short.MAX_VALUE);
        MAX_VALUES.put(IntegerType.class, Integer.MAX_VALUE);
        MAX_VALUES.put(LongType.class, Long.MAX_VALUE);
    }

    /**
     * Get the native identifier generator class.
     * @return TableNameSequenceGenerator.
     */
    @Override
    public Class<?> getNativeIdentifierGeneratorClass() {
        return TableNameSequenceGenerator.class;
    }

    /**
     * Creates a sequence per table instead of the default behavior of one
     * sequence.
     */
    public static class TableNameSequenceGenerator extends SequenceGenerator {

        /**
         * {@inheritDoc} If the parameters do not contain a
         * {@link SequenceGenerator#SEQUENCE} name, we assign one based on the
         * table name.
         */
        @Override
        public void configure(final Type type, final Properties params, final Dialect dialect) {
            if (params.getProperty(SEQUENCE) == null || params.getProperty(SEQUENCE).length() == 0) {
                /* Sequence per table */
                String tableName = params.getProperty(PersistentIdentifierGenerator.TABLE);
                if (tableName != null) {
                    params.setProperty(SEQUENCE, createSequenceName(tableName));
                }

                Number maxValue = getTypeMaxValue(type);

                /* Non-Caching Sequence */
                params.setProperty(PARAMETERS, String.format(CustomOracleDialect.PARAMETERS,
                        maxValue != null ? maxValue.toString() : DEFAULT_MAX_VALUE));
            }
            super.configure(type, params, dialect);
        }

        /**
         * Construct a sequence name from a table name.
         * @param tableName the table name
         * @return the sequence name
         */
        String createSequenceName(final String tableName) {
            return "seq_" + tableName;
        }

        /**
         * Gets the maximum numeric value of the specified mapping class.
         * @param type PrimitiveType implementation. Currently supported types are ByteType, ShortType,
         *             IntegerType and LongType.
         * @return A numeric maximum value representation or null if unsupported type is given.
         */
        Number getTypeMaxValue(final Type type) {
            return type instanceof PrimitiveType ? MAX_VALUES.get(type.getClass()) : null;
        }
    }

}
