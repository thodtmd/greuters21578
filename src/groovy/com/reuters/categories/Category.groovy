package com.reuters.categories

import com.reuters.MappingHelper
import org.grails.datastore.mapping.proxy.EntityProxy

/**
 * Abstract Category type.
 */
abstract class Category {

    Short id
    String name
    String description

    static constraints = {
        name(unique: true, maxSize: 50)
        description(nullable: true, maxSize: 500)
    }

    static mapping = {
        id(generator: MappingHelper.identityGeneratorType)
        version(false)
        cache(usage: 'read-only', include: 'non-lazy')
    }

    String toString() {
        return this.name
    }

    int hashCode() {
        return this.class.name.hashCode() + (this.name != null ? this.name.hashCode() : 0)
    }

    boolean equals(Object o) {
        if (o == null) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (o instanceof Category) {
            return (this.class == o.class && this.name == o.name)
        } else if (o instanceof EntityProxy) {
            return this == o.target
        } else {
            return false
        }
    }

}
