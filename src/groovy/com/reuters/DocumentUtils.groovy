package com.reuters

import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Log4j
import groovy.util.slurpersupport.GPathResult
import org.xml.sax.InputSource

import java.nio.file.FileSystems
import java.nio.file.Files
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.sql.DataSource

/**
 * Utility class with various static SGML data processing methods.
 */
@Log4j
class DocumentUtils {

    public static final Comparator<Document> DOCUMENT_ID_COMPARATOR

    private static final String ROOT_OPENING_TAG = "<ROOT>"
    private static final String ROOT_CLOSING_TAG = "</ROOT>"
    private static final String DTD_DECLARATION = "<!DOCTYPE lewis SYSTEM \"lewis.dtd\">"
    private static final String REUTERS_DATE_FORMAT_STRING = "dd-MMM-yyyy HH:mm:ss.SSS"

    private static final DateFormat REUTERS_DATE_FORMAT =
        new SimpleDateFormat(REUTERS_DATE_FORMAT_STRING, Locale.US)

    private static final Pattern CHOPPED_MILLIS_PATTERN =
        Pattern.compile('^\\d{2}-\\w{3}-\\d{4} \\d{2}:\\d{2}:\\d{2}\\.\\d{1,2}$')

    private static final Pattern CHARACTER_ENTITY_PATTERN =
        Pattern.compile('&#(\\d+);')

    static {
        REUTERS_DATE_FORMAT.timeZone = TimeZone.getTimeZone("Etc/UTC")
        DOCUMENT_ID_COMPARATOR = new Comparator<Document>() {

            @Override
            int compare(Document o1, Document o2) {
                return o1.reutersNewId <=> o2.reutersNewId
            }
        }
    }

    /**
     * This shouldn't be instantiated.
     */
    private DocumentUtils() {
        throw new InstantiationException()
    }

    /**
     * Parses a String representation of a Date found in Reuters-21578 data set.
     * @param string a String in form of <i>dd-MMM-yyyy HH:mm:ss.SSS</i>.
     * @return A Date instance or null in case of empty String.
     */
    static Date parseReutersDate(String string) {
        if (string) {
            if (CHOPPED_MILLIS_PATTERN.matcher(string).matches()) {
                return parseReutersDate(string.padRight(REUTERS_DATE_FORMAT_STRING.size(), "0"))
            } else {
                return REUTERS_DATE_FORMAT.parse(string)
            }
        }
        return null
    }

    /**
     * Strips all SGML garbage (encoded C0 and C1 control characters) from a line of text.
     * @param line a text line, possibly with garbage.
     * @param stub a stub to use as a replacement. Defaults to empty String. If null,
     * replaces '&amp;' with '&amp;amp;', leaving the code intact.
     * @return A String with replaced or removed SGML garbage.
     */
    static String stripSGMLLineGarbage(String line, String stub = "") {
        Matcher matcher = CHARACTER_ENTITY_PATTERN.matcher(line)
        StringBuffer buffer = new StringBuffer(line.size())
        while (matcher.find()) {
            int code = Integer.parseInt(matcher.group(1))
            String replacement = isValidXMLCharacterCode(code) ? matcher.group() :
                (stub != null ? stub : "&amp;#$code;")
            matcher.appendReplacement(buffer, replacement)
        }
        matcher.appendTail(buffer)
        return buffer.toString()
    }

    /**
     * Checks if the passed character entity code is legal for a valid XML document.
     * @param code character entity code (i.e. number in &amp;#...;).
     * @return True if the code is valid.
     */
    static boolean isValidXMLCharacterCode(int code) {
        return code >= 32 && code != 127
    }

    /**
     * Convenience method for returning the text representation of an XML entity.
     * @param entity GPathResult XML entity.
     * @param trim whether to trim the output.
     * @return entity.find ( ) .text ( ) output.
     */
    static String entityText(GPathResult entity, boolean trim = true) {
        return trim ? ((GPathResult) entity.find())?.text()?.trim() : ((GPathResult) entity.find())?.text()
    }

    /**
     * Creates a piped SGML preparser for documents from the Reuters-21578 collection. Uses a separate thread to read
     * the data, removing line-by-line all character entities deemed invalid in XML. Encapsulates the SGML with a root tag.
     * @param sgmlFile input SGML file.
     * @param charset input file charset.
     * @return The InputStream end of the pipe.
     */
    static InputStream createSGMLPreparser(File sgmlFile, String charset) {
        def sink = new PipedInputStream()
        def source = new PipedOutputStream(sink)
        def writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(source, charset)))

        Thread.start() {
            try {
                writer.println(ROOT_OPENING_TAG)
                sgmlFile.withReader(charset) { reader ->
                    def firstLine = reader.readLine()
                    if (firstLine != null) {
                        if (firstLine != DTD_DECLARATION) {
                            writer.println(firstLine)
                        }
                        reader.eachLine() { line ->
                            writer.println(stripSGMLLineGarbage(line))
                        }
                    }
                    return
                }
                writer.println(ROOT_CLOSING_TAG)
            } finally {
                writer.close()
            }
        }

        return sink
    }

    /**
     * Convenience method for creating an InputSource from an InputStream with an encoding specified.
     * @param inputStream InputStream instance.
     * @param encoding valid encoding name.
     * @return an instance of InputSource.
     */
    static InputSource newInputSource(InputStream inputStream, String encoding) {
        def result = new InputSource(inputStream)
        result.encoding = encoding
        return result
    }

    /**
     * Lists all files in folder matching glob.
     * @param folder valid file path.
     * @param glob glob to match.
     * @return A list of files.
     */
    static List<File> listFiles(String folder, String glob) {
        def result = []
        def folderPath = FileSystems.getDefault().getPath(folder)
        def directoryStream = Files.newDirectoryStream(folderPath, glob)
        for (def path : directoryStream) {
            result.add(path.toFile())
        }
        return result
    }

    /**
     * Deletes all stored documents using SQL.
     * @param dataSource the datasource with documents.
     */
    static void deleteAllDocumentsWithSQL(DataSource dataSource) {
        def sql = new Sql(dataSource)
        try {
            sql.withTransaction {
                sql.executeUpdate("DELETE FROM DOCUMENT_EXCHANGES")
                sql.executeUpdate("DELETE FROM DOCUMENT_PEOPLE")
                sql.executeUpdate("DELETE FROM DOCUMENT_ORGS")
                sql.executeUpdate("DELETE FROM DOCUMENT_PLACES")
                sql.executeUpdate("DELETE FROM DOCUMENT_TOPICS")
                sql.executeUpdate("DELETE FROM DOCUMENT")
            }
        } finally {
            sql?.close()
        }
    }

    /**
     * Executes the specified query in a transaction, returning all results in a list.
     * @param dataSource the datasource.
     * @param query SQL query.
     * @return A list of GroovyRowResult objects.
     */
    static List<GroovyRowResult> executeSQLInTransaction(DataSource dataSource, String query) {
        def sql = new Sql(dataSource)
        def result = Collections.emptyList()
        try {
            sql.withTransaction {
                result = sql.rows(query)
            }
        } finally {
            sql?.close()
        }
        return result
    }

    /**
     * Creates a deep-copy of a Document (i.e. everything, except id).
     * @param document Document instance to clone.
     * @return A a deep-copy of the specified Document.
     */
    static Document cloneDocument(Document document) {
        if (document) {
            def newDocument = new Document(document.properties)
            document.exchanges.each {
                newDocument.addToExchanges(it)
            }
            document.orgs.each {
                newDocument.addToOrgs(it)
            }
            document.people.each {
                newDocument.addToPeople(it)
            }
            document.places.each {
                newDocument.addToPlaces(it)
            }
            document.topics.each {
                newDocument.addToTopics(it)
            }
            return newDocument
        }
        return null
    }

    /**
     * Builds a list of clones from a base sorted set.
     * @param base a SortedSet with Document entities sorted by reutersNewId.
     * @param offset the reutersNewId after which the new list will begin.
     * @param amount total amount of clones to build.
     * @return A collection of Document clones or an empty list if the base was empty.
     */
    static Collection<Document> buildClones(SortedSet<Document> base, int offset, int amount) {
        if (base && amount > 0 && offset >= 0) {
            if (base.last().reutersNewId <= offset) {
                return buildClones(base, 0, amount)
            }

            def result = [] as LinkedList
            def left = amount
            def iterator = base.iterator()

            if (offset > 0) { // if we do have an offset, skip until the matching ID
                while (iterator.hasNext() && left > 0) {
                    def doc = iterator.next()
                    if (offset >= doc.reutersNewId) {
                        continue
                    }
                    result.add(cloneDocument(doc))
                    left--
                }
            }

            while (left > 0) { // if there's something else left, add until no more left
                iterator = base.iterator()
                while (left > 0 && iterator.hasNext()) {
                    result.add(cloneDocument(iterator.next()))
                    left--
                }
            }
            return result
        }
        return Collections.emptyList()
    }

}
