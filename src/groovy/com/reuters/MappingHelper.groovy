package com.reuters

import grails.util.Holders

class MappingHelper {

    static String getTextType() {
        def driverClassName = Holders.config.dataSource.driverClassName
        switch (driverClassName) {
            case "oracle.jdbc.OracleDriver":
                return "clob"
            default:
                return "text"
        }
    }

    static String getIdentityGeneratorType() {
        def driverClassName = Holders.config.dataSource.driverClassName
        switch (driverClassName) {
            case "oracle.jdbc.OracleDriver":
                return "native"
            default:
                return "identity"
        }
    }

}
