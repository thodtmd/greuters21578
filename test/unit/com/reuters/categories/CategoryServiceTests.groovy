package com.reuters.categories

import com.reuters.MappingHelper
import grails.test.GrailsUnitTestCase

class CategoryServiceTests extends GrailsUnitTestCase {

    def categoryService

    void setUp() {
        super.setUp()

        registerMetaClass(MappingHelper)
        mockMappingHelper()

        mockDomain(Exchange)
        mockDomain(Organization)
        mockDomain(Place)
        mockDomain(Topic)
        mockDomain(Person)

        categoryService = new CategoryService()
    }

    void mockMappingHelper() {
        MappingHelper.metaClass.static.getIdentityGeneratorType = { ->
            return "auto"
        }

        MappingHelper.metaClass.static.getTextType = { ->
            return "text"
        }
    }

    void testGetExchangesFromFile() {
        def allExchanges = categoryService.getCategoriesFromFile(new File("reuters21578/all-exchanges-strings.lc.txt"), Exchange.class)
        assert allExchanges.size() == 39
        assert allExchanges.find().class == Exchange.class
    }

    void testGetOrgsFromFile() {
        def allOrgs = categoryService.getCategoriesFromFile(new File("reuters21578/all-orgs-strings.lc.txt"), Organization.class)
        assert allOrgs.size() == 56
        assert allOrgs.find().class == Organization.class
    }

    void testGetPlacesFromFile() {
        def allPlaces = categoryService.getCategoriesFromFile(new File("reuters21578/all-places-strings.lc.txt"), Place.class)
        assert allPlaces.size() == 175
        assert allPlaces.find().class == Place.class
    }

    void testGetTopicsFromFile() {
        def allTopics = categoryService.getCategoriesFromFile(new File("reuters21578/all-topics-strings.lc.txt"), Topic.class)
        assert allTopics.size() == 135
        assert allTopics.find().class == Topic.class
    }

    void testGetPeopleFromFile() {
        def allPeople = categoryService.getCategoriesFromFile(new File("reuters21578/all-people-strings.lc.txt"), Person.class)
        assert allPeople.size() == 267
        assert allPeople.find().class == Person.class
    }

    void testCategoryNameSize() {
        def allCategories = [] as LinkedHashSet
        allCategories.addAll(categoryService.getCategoriesFromFile(new File("reuters21578/all-exchanges-strings.lc.txt"), Exchange.class))
        allCategories.addAll(categoryService.getCategoriesFromFile(new File("reuters21578/all-orgs-strings.lc.txt"), Organization.class))
        allCategories.addAll(categoryService.getCategoriesFromFile(new File("reuters21578/all-places-strings.lc.txt"), Place.class))
        allCategories.addAll(categoryService.getCategoriesFromFile(new File("reuters21578/all-topics-strings.lc.txt"), Topic.class))
        allCategories.addAll(categoryService.getCategoriesFromFile(new File("reuters21578/all-people-strings.lc.txt"), Person.class))

        assert allCategories.size() == 672
        assert !allCategories.find() { Category category ->
            return category.name.size() > category.constraints.name.maxSize
        }
    }

    void testPersistCategories() {
        def allCategories = [] as LinkedHashSet
        allCategories.addAll(categoryService.getCategoriesFromFile(new File("reuters21578/all-exchanges-strings.lc.txt"), Exchange.class))
        allCategories.addAll(categoryService.getCategoriesFromFile(new File("reuters21578/all-orgs-strings.lc.txt"), Organization.class))
        allCategories.addAll(categoryService.getCategoriesFromFile(new File("reuters21578/all-places-strings.lc.txt"), Place.class))
        allCategories.addAll(categoryService.getCategoriesFromFile(new File("reuters21578/all-topics-strings.lc.txt"), Topic.class))
        allCategories.addAll(categoryService.getCategoriesFromFile(new File("reuters21578/all-people-strings.lc.txt"), Person.class))

        categoryService.persistCategories(allCategories)
        assert allCategories.size() == (Exchange.count() + Person.count() + Topic.count() + Organization.count() + Place.count())
    }

}
