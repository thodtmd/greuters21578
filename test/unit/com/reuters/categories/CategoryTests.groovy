package com.reuters.categories

import com.reuters.MappingHelper
import grails.test.GrailsUnitTestCase
import org.grails.datastore.mapping.proxy.EntityProxy

class CategoryTests extends GrailsUnitTestCase {

    void setUp() {
        super.setUp()

        registerMetaClass(MappingHelper)
        mockMappingHelper()
    }

    void mockMappingHelper() {
        MappingHelper.metaClass.static.getIdentityGeneratorType = { ->
            return "auto"
        }

        MappingHelper.metaClass.static.getTextType = { ->
            return "text"
        }
    }

    void testHashCode() {
        assert new Exchange(name: "test").hashCode() != new Person(name: "test").hashCode()
        assert new Organization().hashCode() == new Organization().hashCode()
        assert new Topic(name: "test").hashCode() != new Topic(name: "test2").hashCode()
    }

    void testEquals() {
        def exchange = new Exchange(name: "test")
        def person = new Person(name: "test")
        def exchangeList = Collections.singletonList(exchange)
        def personProxy = [
                initialize: {
                },
                getTarget: {
                    return new Person(name: "test")
                },
                isInitialized: {
                    return true
                },
                getProxyKey: {
                    return null
                }
        ] as EntityProxy

        assert exchange != person
        assert exchange != null
        assert exchange == new Exchange(name: "test")
        assert exchange == exchangeList.find()

        assert person == personProxy
    }

}
