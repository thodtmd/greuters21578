package com.reuters

import grails.test.GrailsUnitTestCase

class DocumentUtilsTests extends GrailsUnitTestCase {

    void testParseReutersDate() {
        assert 0L == DocumentUtils.parseReutersDate("01-JAN-1970 00:00:00.00").time
        assert 10L == DocumentUtils.parseReutersDate("01-JAN-1970 00:00:00.01").time
        assert 100L == DocumentUtils.parseReutersDate("01-JAN-1970 00:00:00.1").time

        assert !DocumentUtils.parseReutersDate("")
        assert !DocumentUtils.parseReutersDate(null)
    }

    void testStripSGMLLineGarbage() {
        assert "&#37;Hello world!" == DocumentUtils.stripSGMLLineGarbage("&#37;&#5;Hello &#20;world!")
        assert "Nothing here!" == DocumentUtils.stripSGMLLineGarbage("Nothing here!")
        assert "&#37;&amp;#5;Hello &amp;#20;world!" == DocumentUtils.stripSGMLLineGarbage("&#37;&#5;Hello &#20;world!", null)
    }

    void testCreateSGMLPreparser() {
        def preparedStream = null
        def correctLines = new File("test/unit/com/reuters/testdata/test-prepared.sgm").readLines("UTF-8")
        try {
            preparedStream = DocumentUtils.createSGMLPreparser(new File("test/unit/com/reuters/testdata/test-raw.sgm"), "UTF-8")
            preparedStream.readLines("UTF-8").eachWithIndex { String line, int i ->
                assert correctLines[i] == line
            }
        } finally {
            preparedStream?.close()
        }
    }

    void testListFiles() {
        def tempFile = File.createTempFile("TEST", ".tmp")
        tempFile << "TEST"

        def files = DocumentUtils.listFiles(System.getProperty("java.io.tmpdir"), "*.tmp")
        assert files.find() {
            return it.path == tempFile.path
        }
    }

}